(local fm (require :fullmoon))

(local listen (or (os.getenv :WMIA_LISTEN) "127.0.0.1"))
(local port   (or (os.getenv :WMIA_PORT) "9090"))
(local url    (or (os.getenv :WMIA_URL) "http://localhost"))

(local clients {:curl true
                :wget true
                :httpie true})

;; Macro for creating a mixed table
;; Source: https://github.com/bakpakin/Fennel/issues/353#issuecomment-840015752
(macro mix [...]
  (let [args [...]
        t    {}]
    (var j 0)
    (each [i x (ipairs args) :until (= x '&)]
      (set j i)
      (table.insert t x))
    (for [i (+ j 2) (length args) 2]
      (tset t (. args i) (. args (+ i 1))))
    t))

;; check for X-Real-IP first then X-Forwarded-For
(fn getRequesterIp [headers]
  (or
    (. headers :x-real-ip)
    (. headers :x-forwarded-for)
    "127.0.0.1"))

;; request handlers
(local handlers {:plain #(getRequesterIp (. $1 :headers))
                 :json  #(fm.serveContent :json {:ip (getRequesterIp (. $1 :headers))})
                 :html  #(fm.serveContent :index {:ip (getRequesterIp (. $1 :headers))
                                                  :url url})})

;; loading template(s)
(fm.setTemplate (mix "/views/" & :tmpl "fmt"))

;; serving static files
(fm.setRoute "/static/*" fm.serveAsset)
(fm.setRoute "/favicon.ico" fm.serveAsset)

;; allowed formats are /json, /plain and /html
(fm.setRoute (mix "/:format" & :method "GET" :otherwise 403) (fn [req]
  (let [handler (. handlers (. req :params :format))]
    (if (not handler) fm.serve403 (handler req)))))

;; / returns the html page unless the user-agent is one of the defined cli agents
(fm.setRoute (mix "/" & :method "GET" :otherwise 403) (fn [req]
  (let [rawUserAgent  (. req :headers :user-agent)
        userAgent     (string.match (or rawUserAgent "") "^(%w+)/.*$")]
    (if (not= (. clients (userAgent:lower)) nil)
      ((. handlers :plain) req)
      ((. handlers :html) req)))))

(fm.run {:addr listen :port port})
