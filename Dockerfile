FROM alpine:latest AS build

LABEL maintainer="Sebastian Huebner <sh@kokolor.es>"

ARG version="dev"

COPY wmia-${version}.com /wmia.com
RUN wget https://cosmo.zip/pub/cosmos/bin/assimilate && chmod +x assimilate && ./assimilate wmia.com

FROM scratch

COPY --from=build /wmia.com /
CMD ["/wmia.com"]
