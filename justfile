# Dependencies
## dependencies dir

dep_dir := "./deps"
lua_deps_dir := dep_dir / ".lua"
web_deps_dir := dep_dir / "static"

## redbean single-file webserver

redbean_version := "3.0.0"
redbean_bin := dep_dir / "redbean.com"
redbean_dl_url := "https://redbean.dev/redbean-" + redbean_version + ".com"

## fennel compiler

fennel_version := "1.5.1"
fennel_bin := dep_dir / "fennel"
fennel_dl_url := "https://fennel-lang.org/downloads/fennel-" + fennel_version + "-x86_64"

## Fullmoon web framework

fullmoon_version := "master"
fullmoon_module := lua_deps_dir / "fullmoon.lua"
fullmoon_dl_url := "https://raw.githubusercontent.com/pkulchenko/fullmoon/refs/heads/" + fullmoon_version + "/fullmoon.lua"

# Vars for build

build_dir := `mktemp -d --tmpdir=.`
default_version := "dev"

default: build

[doc("Build wmia web app")]
build version=default_version:
    @echo "Building wmia-{{ version }}.com"
    -[ -f ./wmia-{{ version }}.com ] && rm ./wmia-{{ version }}.com
    {{ fennel_bin }} -c wmia.fnl > {{ build_dir }}/.init.lua
    cp -r ./static {{ build_dir }}/static
    cp -r ./views {{ build_dir }}/views
    cp -r {{ lua_deps_dir }} {{ build_dir }}/.lua
    cp {{ redbean_bin }} ./wmia-{{ version }}.com
    cd {{ build_dir }} && zip -r ../wmia-{{ version }}.com $(ls -A)
    rm -r {{ build_dir }}

[doc("Build and run wmia web app")]
run version=default_version: (build version)
    chmod +x ./wmia-{{ version }}.com
    -LUA_PATH="/zip/.lua/?.lua;/zip/.lua/?/init.lua" ./wmia-{{ version }}.com

# Project preparation / Dependency setup

curl_cmd := "curl -s -L -o"
redbean_bin_exists := path_exists(redbean_bin)
fennel_bin_exists := path_exists(fennel_bin)
fullmoon_module_exists := path_exists(fullmoon_module)

[doc("Download all the needed dependencies")]
[group("prepare")]
prepare: create_dirs get_redbean get_fennel get_fullmoon

[group("prepare")]
create_dirs:
    mkdir -p {{ dep_dir }}
    mkdir -p {{ lua_deps_dir }}

[group("prepare")]
get_fennel force="false":
    if [ {{ force }} = "true" ] || [ {{ fennel_bin_exists }} = "false" ]; then \
        {{ curl_cmd }} {{ fennel_bin }} {{ fennel_dl_url }}; \
        chmod +x {{ fennel_bin }}; \
    fi

[group("prepare")]
get_fullmoon force="false":
    if [ {{ force }} = "true" ] || [ {{ fullmoon_module_exists }} = "false" ]; then \
        {{ curl_cmd }} {{ fullmoon_module }} {{ fullmoon_dl_url }}; \
    fi

[group("prepare")]
get_redbean force="false":
    if [ {{ force }} = "true" ] || [ {{ redbean_bin_exists }} = "false" ]; then \
        {{ curl_cmd }} {{ redbean_bin }} {{ redbean_dl_url }}; \
        chmod +x {{ redbean_bin }}; \
    fi

[group("cleanup")]
cleanup_deps:
    rm -r {{ dep_dir }}
