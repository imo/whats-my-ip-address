# whats-my-ip-address

A tiny "What's my IP address" - service written in [Fennel](https://fennel-lang.org/), using the [redbean](https://redbean.dev/) webserver and the [Fullmoon](https://github.com/pkulchenko/fullmoon) web framework. The frontend is inspired by the http://bettermotherfuckingwebsite.com/ and https://perfectmotherfuckingwebsite.com.

## Warning

I program for fun and to learn, so please use this at your own risk. If you see something that is totally wrong, very strangely implemented or could be done better, I would be happy if you tell me or create a pull request. Thanks :)

## Installation

### Binary

* download the latest version from [releases](https://git.kokolor.es/imo/whats-my-ip-address/releases)
* make it executable `chmod +x`
* run it
```
$ ./wmia.com
I2025-02-21T15:34:50.206027:/zip/.lua/fullmoon.lua:63:wmia-dev:200716] (fm) started redbean/3.0.0 fullmoon/0.384
I2025-02-21T15:34:50+000346:tool/net/redbean.c:7052:wmia-dev:200716] (srvr) listen http://127.0.0.1:9090
```
* there are three environment variables to configure the service:
```
WMIA_LISTEN=127.0.0.1      # listen address
WMIA_PORT=9090             # listen port
WMIA_URL=http://localhost  # the URL displayed in the examples
```
* I recommand to run it behind a reverse proxy
* an example nginx config and systemd service file can be found in [install/](install/)

### Docker

Docker container images can be pulled from:
* Docker Hub: [imolein/wmia](https://hub.docker.com/repository/docker/imolein/wmia)
* My Gitea: [imo/wmia](https://git.kokolor.es/imo/-/packages/container/wmia)

```
$ docker run -p 9090:9090 -e "WMIA_LISTEN=0.0.0.0" -it --rm git.kokolor.es/imo/wmia:1.0
I2025-02-25T12:05:57.161693:/zip/.lua/fullmoon.lua:63:wmia:1] (fm) started redbean/3.0.0 fullmoon/0.384
I2025-02-25T12:05:57+001786:tool/net/redbean.c:7052:wmia:1] (srvr) listen http://127.0.0.1:9090
```

## Building

**Dependencies:**
* [Fennel](https://fennel-lang.org/)
* [redbean](https://redbean.dev/)
* [Fullmoon](https://github.com/pkulchenko/fullmoon)
* [Just](https://just.systems)
* curl
* zip

```
$ just -l
Available recipes:
    build version=default_version # Build wmia web app
    default
    run version=default_version   # Build and run wmia web app

    [cleanup]
    cleanup_deps

    [prepare]
    create_dirs
    get_fennel force="false"
    get_fullmoon force="false"
    get_redbean force="false"
    prepare                       # Download all the needed dependencies
```
